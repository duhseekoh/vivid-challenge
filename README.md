## Other thing's I would add

### redux
for state management
follow redux module approach (/module-name/actions.js|reducer.js|selectors.js)
store all entities retrieved through api calls
store some level of ui state (e.g. loading, saving, what entities to display on what pages)

### redux-thunk
to handle side effects

### flow
provide typings for all component props
add a types.js file in each redux module

### react-router
build out a pages folder structure
container components can be the screens themselves, which can contain other container components

### api client
start with an api folder which contains methods for each api call, configurable with params
separate that out into a separate api client package after a while

### redux-form
for anything more complex than a couple of text inputs

### ui framework
material ui or semantic ui
depending on designer resources, just use material ui out of the box with some theming.

### jest
for unit and component testing
