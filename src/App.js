import React, { Component } from 'react';
import './App.css';
import EventRoute from './routes/events/EventRoute';

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* TODO replace with react router in future */}
        <EventRoute />
      </div>
    );
  }
}

export default App;
