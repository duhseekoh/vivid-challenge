import React, { Component } from 'react';
import EventList from './components/event-list/EventList';
import AddEventModal from './components/add-event-modal/AddEventModal';
import api from '../../api/api';

class EventRoute extends Component {

  constructor(props) {
    super(props);

    // TODO - setup state
    this.state = {
      events: null,
      hasError: false,
      isModalOpen: false,
    }
  }

  componentDidMount() {
    this.refreshEvents();
  }

  refreshEvents = () => {
    api.eventService.all((events) => {
      // success
      this.setState({
        events,
        hasError: false,
      });
    }, (error) => {
      this.setState({
        hasError: true,
      });
    });
  }

  handleDisplayAddEventModal = () => {
    this.setState({
      isModalOpen: true,
    });
  }

  handleAddEventSubmit = (event) => {
    event.preventDefault();
    const formInputs = document.getElementById('add-event-form').elements;
    console.log(formInputs);
    const values = {
      name: formInputs[0].value,
      date: formInputs[1].value,
      venue: {
        name: formInputs[2].value,
        city: formInputs[3].value,
        state: formInputs[4].value,
      }
    };
    console.log(values);
    api.eventService.add(values, () => {
      this.refreshEvents();
    }, () => {
      alert('failed to submit! try again');
    });
    // const formValues = formInputs.map(fi => fi.value);
    // console.log(formValues);
  }

  render() {
    const { events, hasError, isModalOpen } = this.state;

    if (hasError) {
      return (
        <div>
          Sorry, server error
          {/* TODO button to retry */}
        </div>
      )
    }

    // TODO - replace with loading state var
    if (!events) {
      return null;
    }

    return (
      <div>
        {/* <EventListFilters onSelectFilter={}/> */}
        <EventList events={events} />
        <button type='button' onClick={this.handleDisplayAddEventModal}>Add Event</button>

        { isModalOpen && (
          <AddEventModal onSubmit={this.handleAddEventSubmit} />
        ) }
      </div>
    )
  }

}

export default EventRoute;
