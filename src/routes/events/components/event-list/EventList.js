import React from "react";

const EventListItem = ({ event }) => (
  <tr>
    <td>{event.name}</td>
    <td>{event.date}</td>
    <td>{event.venue.name}</td>
    <td>{event.venue.city}</td>
    <td>{event.venue.state}</td>
  </tr>
);

export default ({ events }) => {
  return (
    <table class="table events-table">
      <tr class="events-table-header">
        <th>Event Name</th>
        <th>Date</th>
        <th>Venue</th>
        <th>City</th>
        <th>State</th>
      </tr>
      {events && events.map(ev => <EventListItem event={ev} />)}
    </table>
  );
};
